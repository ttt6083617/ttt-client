import { useEffect, useState } from "react";

import { getHours, getMins, getSecs } from "utils/utils";
import { Dropdown } from "features/generics/dropdown/Dropdown";
import { groupBy } from "lodash";
import { TasksList } from "features/tasksList/TasksList";

const projects = ['ttt-client', 'ttt-api'];

export const Dashboard = () => {
    const [isActive, setIsActive] = useState(false);
    const [time, setTime] = useState(0);
    const [desc, setDesc] = useState(() => '');
    const [project, setProject] = useState(() => '');
    const [entries, setEntries] = useState(() => []);
    const [groupedEntries, setGroupedEntries] = useState(() => { });

    useEffect(() => {
        let interval = null;

        if (isActive) {
            interval = setInterval(() => {
                setTime((time) => time + 1);
            }, 1000);
        } else {
            clearInterval(interval);
        }
        return () => {
            clearInterval(interval);
        };
    }, [isActive]);

    const handleStart = () => {
        setIsActive(true);
    };

    const handleStop = () => {
        setIsActive(false);
        addEntry();
        setTime(0);
        setDesc('');
        setProject('');
    };

    const addEntry = () => {
        const entry = {
            id: crypto.randomUUID(),
            time,
            desc,
            project,
            date: new Date().toLocaleDateString(),
        };
        setEntries(arr => [entry, ...arr]);
    };

    const handleDDSelItem = item => {
        setProject(item);
    };

    useEffect(() => {
        if (entries.length) {
            setGroupedEntries(groupBy(entries, 'date'));
        } else {
            setGroupedEntries({});
        }
    }, [entries]);

    return (
        <>
            <button type="button" onClick={isActive ? handleStop : handleStart}>start/stop</button>

            <span>{`${getHours(time)}:${getMins(time)}:${getSecs(time)}`}</span>
            <Dropdown menu={projects} handleDDSelItem={handleDDSelItem} selItem={project} />
            <input type="text" value={desc} onChange={e => setDesc(e.target.value)} />

            <div>time - project - description</div>
            
            <TasksList groupedEntries={groupedEntries} />
        </>
    )
}