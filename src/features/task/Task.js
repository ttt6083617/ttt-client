import { getHours, getMins, getSecs } from "utils/utils";

export const Task = ({ entry }) => {
    return (
        <div>
            <span>{`${getHours(entry.time)}:${getMins(entry.time)}:${getSecs(entry.time)}`} - {entry.project} - {entry.desc}</span>
            <button type="button" onClick={() => { }}>x</button>
        </div>
    )
}