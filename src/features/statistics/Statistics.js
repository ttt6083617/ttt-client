import { useCallback, useState } from "react";

export const Statistics = () => {
    const [selDate, setSelDate] = useState(() => '');

    const chSelDate = useCallback(e => {
        setSelDate(e.target.value);
    }, [setSelDate]);

    return (
        <>
            <input type="date" value={selDate} onChange={chSelDate} />
        </>
    )
}