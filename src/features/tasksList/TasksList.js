import { Fragment } from "react";
import { map } from "lodash";

import { Task } from "features/task/Task";

export const TasksList = ({ groupedEntries }) => {
    return (
        <>
            {map(groupedEntries && groupedEntries, (entryArr, idx) => {
                return (
                    <Fragment key={idx}>
                        {entryArr[0].date}
                        {map(entryArr, entry => {
                            return (
                                <Task entry={entry} key={entry.id} />
                            )
                        })}
                    </Fragment>
                )
            })}
        </>
    )
}