import { useState } from "react";

export const Dropdown = ({ menu, handleDDSelItem, selItem }) => {
    const [open, setOpen] = useState(false);

    const handleOpen = () => {
        setOpen(!open);
    };

    const handleSelectItem = e => {
        handleDDSelItem(e.target.textContent);
        setOpen(false);
    };

    return (
        <span>
            <button type="button" onClick={handleOpen}>{selItem || 'dropdown'}</button>
            {open ? (
                <ul>
                    {menu.map((item, idx) => (
                        <li key={idx}>
                            <button type="button" onClick={handleSelectItem}>{item}</button>
                        </li>
                    ))}
                </ul>
            ) : null}
        </span>
    );
};