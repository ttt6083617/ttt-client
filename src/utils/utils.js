export function getHours(time) {
    return ("0" + Math.floor((time / 3600) % 60)).slice(-2);
}

export function getMins(time) {
    return ("0" + Math.floor((time / 60) % 60)).slice(-2);
}

export function getSecs(time) {
    return ("0" + Math.floor((time) % 60)).slice(-2);
}